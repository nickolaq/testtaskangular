import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { gameCatalog } from './game-catalog/game-catalog.component';

@NgModule({
  declarations: [
    gameCatalog
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [gameCatalog]
})
export class AppModule { }
