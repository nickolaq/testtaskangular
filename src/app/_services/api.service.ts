import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public data: any;
  private REST_API_SERVER = "/src/data/games.json";

  constructor(private httpClient: HttpClient) {
  }

  public getGamesRequest() {
    return this.httpClient.get(this.REST_API_SERVER);
  }
}
