import {Component} from '@angular/core';
import {ApiService} from '../_services/api.service';
import {
    orderBy as _orderBy,
    keys as _keys,
    remove as _remove,
    includes as _includes,
    uniq as _uniq,
    concat as _concat
} from 'lodash';
import {forEach} from '@angular/router/src/utils/collection';

@Component({
    selector: 'game-catalog',
    templateUrl: './game-catalog.component.html',
    styleUrls: ['./game-catalog.component.sass']
})
export class gameCatalog {
    public filtersName: string[] = [
        'Name',
        'ID',
        'MerchantID'
    ];
    protected data: any;
    protected merchants: any;
    protected categories: any;
    protected games: any;
    protected sortedGames: any = [];
    protected showFavouritesGames: boolean = false;
    protected limit: number = 5;
    protected favouritesGames: any = [];

    constructor(private apiService: ApiService) {
    };

    ngOnInit() {
        this.apiService.getGamesRequest().subscribe((data: any) => {
            if (data) {
                this.data = data;
                this.games = this.data.games;
                this.categories = this.data.categories;
                this.merchants = this.data.merchants;
                this.sortedGames = this.games;
                this.sortGamesByParams();
            }
        });
        if (localStorage.getItem('favourites')) {
            this.favouritesGames = JSON.parse(localStorage.getItem('favourites'));
        }
        if (localStorage.getItem('sortedFav')) {
            this.sortedGames = JSON.parse(localStorage.getItem('sortedFav'));
        }
    }

    protected filterByParams(param: string = 'all', value: string): void {
        if (param === 'category') {
            this.sortedGames = this.games.filter((el: any) => {
                return _includes(el.CategoryID, value);
            });
        } else if (param === 'merchant') {
            this.sortedGames = this.games.filter((el: any) => {
                return _includes(el.MerchantID, value);
            });
        } else {
            this.sortedGames = this.games;
        }
        this.showFavouritesGames = false;
        this.sortGamesByParams();
    }

    protected sortGamesByParams(paramName: string = 'Name.en', order?: any) {
        paramName = paramName === 'Name' ? 'Name.en' : paramName;
        this.sortedGames.forEach(el => {
            if (_includes(this.favouritesGames, el)) {
                el.favClass = 'fav';
            } else {
                el.favClass = '';
            }
        });
        this.sortedGames = _orderBy(this.sortedGames, [paramName], [order]);
        localStorage.setItem('sortedFav', JSON.stringify(this.sortedGames));
    }

    protected showMoreGames(limitCounter: number = 5): void {
        this.limit += limitCounter;
    }

    protected toggleFavourites(game: any, element: any): void {
        if (element.classList.contains('fav')) {
            element.classList.remove('fav');
        } else {
            element.classList.add('fav');
        }

        if (_includes(this.favouritesGames, game)) {
            _remove(this.favouritesGames, (el: any) => {
                return el.ID === game.ID;
            });
        } else {
            this.favouritesGames.push(game);
        }
        this.favouritesGames = _uniq(this.favouritesGames);
        localStorage.setItem('favourites', JSON.stringify(this.favouritesGames));
    }

    protected orderWithCustomParams(params: string[]): void {
        const tempList = this.sortedGames.filter(el => {
            return !_includes(params, el.ID);
        });
        this.sortedGames = _concat(params, tempList);
    }
}
